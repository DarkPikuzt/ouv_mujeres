<?php
session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass); 
mysqli_select_db($conexion, $datab) or die("error en la conexión");

$grafica=$_POST['grafica'];






switch ($grafica) {
	case 'v1_g1':
		$meses=$_SESSION['meses'];
		$valores=$_SESSION['valores'];

		//var_dump($valores);
		?>
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
		<tr>
			<th></th>
			<?php for($i=0;$i<=5;$i++){ ?>
			<th><?php echo $meses[$i]; ?></th>
			<?php } ?>
			<th>Total</th>
			<th>Mapa</th>
		</tr>
		<tr>
			<th>Región Norte</th>
			<?php $sumaNorte = 0;  ?>
			<?php for($i=0;$i<=5;$i++){ ?>
			<td><?php echo $valores[$i]; ?></td>
			<?php $sumaNorte = $sumaNorte + $valores[$i]; ?>
			<?php } ?>
			<td><?php echo $sumaNorte ?></td>
			<td> <input hidden id="valorC" value="<?php echo $sumaNorte?>">
			<button type="button" class="btn btn-large btn-block btn-default" id="regionNorte"><i class="fa fa-globe" aria-hidden="true"></i></button>
			</td>
		</tr>
		<tr>
			<th>Región Centro</th>
			<?php $sumaCentro = 0;  ?>
			<?php for($i=6;$i<=11;$i++){ ?>
			<td><?php echo $valores[$i]; ?></td>
			<?php $sumaCentro = $sumaCentro + $valores[$i]; ?>
			<?php } ?>
			<td><?php echo $sumaCentro ?></td>
			<td> <input hidden id="valorC" value="<?php echo $sumaCentro ?>">
			<button type="button" class="btn btn-large btn-block btn-default" id="regionCentro" ><i class="fa fa-globe" aria-hidden="true"></i></button>
			</td>
		</tr>
		<tr>
			<th>Región Sur</th>
			<?php $sumaSur = 0;  ?>
			<?php for($i=12;$i<=17;$i++){ ?>
			<td><?php echo $valores[$i]; ?></td>
			<?php $sumaSur = $sumaSur + $valores[$i]; ?>
			<?php } ?>
			<td ><?php echo $sumaSur ?></td>
			<td> <input hidden id="valor" value="<?php echo $sumaSur ?>">
			<button type="button" class="btn btn-small btn-block btn-default" id="regionSur" value="<?php $sumaSur ?>"><i class="fa fa-globe" aria-hidden="true"></i></button>
			</td>
		</tr>
		<tr>
			<th>Estatal</th>
			<td><?php echo $julio= $valores[0]+$valores[6]+$valores[12];?></td>
			<td><?php echo $agosto = $valores[1]+$valores[7]+$valores[13];?></td>
			<td><?php echo $septiembre = $valores[2]+$valores[8]+$valores[14];?></td>
			<td><?php echo $octubre = $valores[3]+$valores[9]+$valores[15];?></td>
			<td><?php echo $nov = $valores[4]+$valores[10]+$valores[16];?></td>
			<td><?php echo $dic = $valores[5]+$valores[11]+$valores[17];?></td>
			<td><?php echo $julio+ $agosto + $septiembre + $octubre + $nov + $dic ?></td>
			
		</tr>		
		</table>
		<?php
	break;
	

	case 'v1_g2':
		$meses=$_SESSION['meses2'];
		$valores=$_SESSION['valores2'];
		?>
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
		<tr>
			<th></th>
			<?php for($i=0;$i<=5;$i++){ ?>
			<th><?php echo $meses[$i]; ?></th>
			<?php } ?>
		</tr>
		<tr>
			<th>Región Norte</th>
			<?php for($i=0;$i<=15;$i+=3){ ?>
			<td><?php echo $valores[$i]; ?></td>
			<?php } ?>
		</tr>
		<tr>
			<th>Región Centro</th>
			<?php for($i=1;$i<=16;$i+=3){ ?>
			<td><?php echo $valores[$i]; ?></td>
			<?php } ?>
		</tr>
		<tr>
			<th>Región Sur</th>
			<?php for($i=2;$i<=17;$i+=3){ ?>
			<td><?php echo $valores[$i]; ?></td>
			<?php } ?>
		</tr>
		<tr>
			<th>Estatal</th>
			<td><?php echo $valores[0]+$valores[1]+$valores[2];?></td>
			<td><?php echo $valores[3]+$valores[4]+$valores[5];?></td>
			<td><?php echo $valores[6]+$valores[7]+$valores[8];?></td>
			<td><?php echo $valores[9]+$valores[10]+$valores[11];?></td>
			<td><?php echo $valores[12]+$valores[13]+$valores[14];?></td>
			<td><?php echo $valores[15]+$valores[16]+$valores[17];?></td>
		</tr>		
		</table>
		<?php
	break;
	case 'v1_g4':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Menor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Adulta'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Adulta mayor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Menor de edad</th>
				<th>Adulta</th>
				<th>Adulta mayor</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
			</tr>
		</table>
	<?php
	break;

	case 'v1_g4-2':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Menor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Adulta'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Adulta mayor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Menor de edad</th>
				<th>Adulta</th>
				<th>Adulta mayor</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
				<td></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
			</tr>
		</table>
	<?php
	break;

	case 'v1_g5':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND t_agresor='Hombre'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND t_agresor='Mujer'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND t_agresor='Grupo de personas'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND t_agresor='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Hombre</th>
				<th>Mujer</th>
				<th>Hombre y Mujer</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
			</tr>
		</table>
	<?php
	break;
	case 'v1_g5-2':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND t_agresor='Hombre'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND t_agresor='Mujer'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND t_agresor='Grupo de personas'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND t_agresor='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Hombre</th>
				<th>Mujer</th>
				<th>Hombre y Mujer</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
			</tr>
		</table>
	<?php
	break;

	case 'v1_g7':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND modalidad='Familiar'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND modalidad='Comunidad'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND modalidad='Escolar'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND modalidad='Institucional'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

        $valores5 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND modalidad='Feminicida'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores5, $value[0]);
        }

		$valores6 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND modalidad='Género'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores6, $value[0]);
        }

		$valores7 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND modalidad='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores7, $value[0]);
        }
       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Familiar o doméstica</th>
				<th>Comunidad</th>
				<th>Escolar</th>
				<th>Institucional</th>
				<th>Feminicida</th>
				<th>Género</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
				<td><?php echo $valores5[0];?></td>
				<td><?php echo $valores6[0];?></td>
				<td><?php echo $valores7[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
				<td><?php echo $valores5[1];?></td>
				<td><?php echo $valores6[1];?></td>
				<td><?php echo $valores7[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
				<td><?php echo $valores5[2];?></td>
				<td><?php echo $valores6[2];?></td>
				<td><?php echo $valores7[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
				<td><?php echo $valores5[0]+$valores5[1]+$valores5[2];?></td>
				<td><?php echo $valores6[0]+$valores6[1]+$valores6[2];?></td>
				<td><?php echo $valores7[0]+$valores7[1]+$valores7[2];?></td>
			</tr>
		</table>
	<?php
	break;

	/* Para la tabla del segundo semestre de violencias por confinamiento*/
	case 'v1_g7-2':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND modalidad='Familiar'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND modalidad='Comunidad'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND modalidad='Escolar'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND modalidad='Institucional'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

        $valores5 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND modalidad='Feminicida'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores5, $value[0]);
        }

		$valores6 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND modalidad='Género'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores6, $value[0]);
        }

		$valores7 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND modalidad='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores7, $value[0]);
        }
       	
		?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Familiar o doméstica</th>
				<th>Comunidad</th>
				<th>Escolar</th>
				<th>Institucional</th>
				<th>Feminicida</th>
				<th>Género</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
				<td><?php echo $valores5[0];?></td>
				<td><?php echo $valores6[0];?></td>
				<td><?php echo $valores7[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
				<td><?php echo $valores5[1];?></td>
				<td><?php echo $valores6[1];?></td>
				<td><?php echo $valores7[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
				<td><?php echo $valores5[2];?></td>
				<td><?php echo $valores6[2];?></td>
				<td><?php echo $valores7[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
				<td><?php echo $valores5[0]+$valores5[1]+$valores5[2];?></td>
				<td><?php echo $valores6[0]+$valores6[1]+$valores6[2];?></td>
				<td><?php echo $valores7[0]+$valores7[1]+$valores7[2];?></td>
			</tr>
		</table>
	<?php
	break;

	/*Cierre */

	case 'v2_g3':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Menor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Adulta'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Adulta mayor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Menor de edad</th>
				<th>Adulta</th>
				<th>Adulta mayor</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
			</tr>
		</table>
	<?php
	break;

		case 'v2_g3-2':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Menor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Adulta'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Adulta mayor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Menor de edad</th>
				<th>Adulta</th>
				<th>Adulta mayor</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
			</tr>
		</table>
	<?php
	break;


	case 'v3_g3':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Menor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Adulta'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Adulta mayor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Menor de edad</th>
				<th>Adulta</th>
				<th>Adulta mayor</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
			</tr>
		</table>
	<?php
	break;
	case 'v3_g3-2':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Menor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Adulta'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Adulta mayor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Menor de edad</th>
				<th>Adulta</th>
				<th>Adulta mayor</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
			</tr>
		</table>
	<?php
	break;

	case 'v3_g4':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND loc_vic='Privado'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND loc_vic='Publico'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND loc_vic='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Privado</th>
				<th>Público</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
			</tr>
		</table>
	<?php
	break;
	case 'v3_g4-2':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND loc_vic='Privado'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND loc_vic='Publico'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_femini WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND loc_vic='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Privado</th>
				<th>Público</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
			</tr>
		</table>
	<?php
	break;

	case 'v4_g3':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Menor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Adulta'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='Adulta mayor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND edad_cat='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Menor de edad</th>
				<th>Adulta</th>
				<th>Adulta mayor</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
			</tr>
		</table>
	<?php
	break;
		case 'v4_g3-2':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Menor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Adulta'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='Adulta mayor'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }

		$valores4 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND edad_cat='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores4, $value[0]);
        }

       	
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Menor de edad</th>
				<th>Adulta</th>
				<th>Adulta mayor</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
				<td><?php echo $valores4[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
				<td><?php echo $valores4[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
				<td><?php echo $valores4[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
				<td><?php echo $valores4[0]+$valores4[1]+$valores4[2];?></td>
			</tr>
		</table>
	<?php
	break;

	case 'v4_g4':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND loc_vic='Privado'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND loc_vic='Publico'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND loc_vic='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Privado</th>
				<th>Público</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
			</tr>
		</table>
	<?php
	break;

	case 'v4_g4-2':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND loc_vic='Privado'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND loc_vic='Publico'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

		$valores3 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_homici WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND loc_vic='No se especifica'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores3, $value[0]);
        }
	?>       	
		<table class="table table-condensed" style="margin: 0 5%; font-size: 12px; width: 90%;">
			<tr>
				<th></th>
				<th>Privado</th>
				<th>Público</th>
				<th>No se especifica</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
				<td><?php echo $valores3[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
				<td><?php echo $valores3[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
				<td><?php echo $valores3[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
				<td><?php echo $valores3[0]+$valores3[1]+$valores3[2];?></td>
			</tr>
		</table>
	<?php
	break;

	case 'v2_g1':
		$norte=$_SESSION['norte'];
		$centro=$_SESSION['centro'];
		$sur=$_SESSION['sur'];
		?>
		<table class="table table-condensed" style="margin: 0 30%; font-size: 12px; width: 40%;">
			<tr>
				<th></th>
				<th>Desapariciones</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $norte;?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $centro;?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $sur;?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $norte+$centro+$sur;?></td>
			</tr>
		</table>
		<?php
		break;

		case 'v3_g1':
		$norte=$_SESSION['norte'];
		$centro=$_SESSION['centro'];
		$sur=$_SESSION['sur'];
		?>
		<table class="table table-condensed" style="margin: 0 30%; font-size: 12px; width: 40%;">
			<tr>
				<th></th>
				<th>Feminicidios</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $norte;?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $centro;?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $sur;?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $norte+$centro+$sur;?></td>
			</tr>
		</table>
		<?php
		break;

		case 'v4_g1':
		$norte=$_SESSION['norte'];
		$centro=$_SESSION['centro'];
		$sur=$_SESSION['sur'];
		?>
		<table class="table table-condensed" style="margin: 0 30%; font-size: 12px; width: 40%;">
			<tr>
				<th></th>
				<th>Homicidios</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $norte;?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $centro;?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $sur;?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $norte+$centro+$sur;?></td>
			</tr>
		</table>
		<?php
		break;
		case 'v2_g4':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND localiza='0'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg<=6 AND localiza='1'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

	?>       	
		<table class="table table-condensed" style="margin: 0 10%; font-size: 12px; width: 80%;">
			<tr>
				<th></th>
				<th>No localizadas</th>
				<th>Localizadas</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
			</tr>
		</table>
	<?php
	break;

	case 'v2_g4-2':
		$anio=$_SESSION['anio'];
		
		$valores1 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND localiza='0'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores1, $value[0]);
        }

		$valores2 = array();
		for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_desap WHERE region='$j' AND anio='$anio' AND mes_reg>=7 AND localiza='1'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores2, $value[0]);
        }

	?>       	
		<table class="table table-condensed" style="margin: 0 10%; font-size: 12px; width: 80%;">
			<tr>
				<th></th>
				<th>No localizadas</th>
				<th>Localizadas</th>
			</tr>
			<tr>
				<th>Región Norte</th>
				<td><?php echo $valores1[0];?></td>
				<td><?php echo $valores2[0];?></td>
			</tr>
			<tr>
				<th>Región Centro</th>
				<td><?php echo $valores1[1];?></td>
				<td><?php echo $valores2[1];?></td>
			</tr>
			<tr>
				<th>Región Sur</th>
				<td><?php echo $valores1[2];?></td>
				<td><?php echo $valores2[2];?></td>
			</tr>
			<tr>
				<th>Estatal</th>
				<td><?php echo $valores1[0]+$valores1[1]+$valores1[2];?></td>
				<td><?php echo $valores2[0]+$valores2[1]+$valores2[2];?></td>
			</tr>
		</table>
	<?php
	break;



	default:
		# code...
		break;
}

?>