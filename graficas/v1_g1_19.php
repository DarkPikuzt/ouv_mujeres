<html>

<?php

session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass);
mysqli_select_db($conexion, $datab) or die("error en la conexión");


//$anio = $_SESSION['anio'];

$anio='2019';
$semestre="1";
//$semestre=array();
$valores = array();
$meses = array();

switch ($semestre) {
  case '1':
      for ($i=1; $i <=3 ; $i++) { 
        for ($j=1; $j <=6 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg='$j' AND anio='$anio' AND region='$i'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores, $value[0]);
        }
      }
      $meses=['Enero','Febrero','Marzo','Abril','Mayo','Junio'];
      $_SESSION['valores']=$valores;
      $_SESSION['meses']=$meses;
    break;
    case '2':
      for ($i=1; $i <=3 ; $i++) { 
        for ($j=7; $j <=12 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg='$j' AND anio='$anio' AND region='$i'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores, $value[0]);
        }
      }
      $meses=['Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
      $_SESSION['valores']=$valores;
      $_SESSION['meses']=$meses;
    break;
}
//}
?>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css"> 
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!-- API PARA DIBUJAR LA TABLA DE DATOS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
      //$(document).ready(function(){
      function tabla(){  
        var grafica='v1_g1';
        $.post("graficas/tablas.php",{ grafica : grafica},function(data){
          $("#tabla").html(data);
        }); 
      }
      //});

       function selecciona() {
        var indice_a = anio_ouv.selectedIndex;
        var valor_a = anio_ouv.options;
        var indice_b = periodo_ouv.selectedIndex;

        //alert("Index: " + valor_a[indice_a].index + "es" + valor_a[indice_a].text);
        /* Para el año 2020*/
        if(indice_a == 0 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v1_g1_20.php');
        } 
        else if(indice_a == 0 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v1_g1_2_20.php');
        }
        /* Para el año 2019*/
        if(indice_a == 1 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v1_g1_19.php');
        } 
        else if(indice_a == 1 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v1_g1_2_19.php');
        }
        /* Para el año 2018*/
        if(indice_a == 2 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v1_g1_18.php');
        } 
        else if(indice_a == 2 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v1_g1_2_18.php');
        }
        /* Para el año 2017*/
        if(indice_a == 3 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v1_g1.php');
        } 
        else if(indice_a == 3 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v1_g1_2.php');
        }


      }


      var meses = <?php echo json_encode($meses)?> ;

      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Región', meses[0], meses[1], meses[2], meses[3], meses[4], meses[5]],
          ['Norte',<?php echo $valores[0];?>, <?php echo $valores[1];?>,<?php echo $valores[2];?>, <?php echo $valores[3];?>, <?php echo $valores[4];?>,<?php echo $valores[5];?>],
          ['Centro', <?php echo $valores[6];?>, <?php echo $valores[7];?>,<?php echo $valores[8];?>, <?php echo $valores[9];?>, <?php echo $valores[10];?>, <?php echo $valores[11];?>],
          ['Sur', <?php echo $valores[12];?>, <?php echo $valores[13];?>, <?php echo $valores[14];?>, <?php echo $valores[15];?>, <?php echo $valores[16];?>, <?php echo $valores[17];?>]
        ]);

        var options = {
          chart: {
            'title': 'Casos de violencia por región',
            'subtitle': 'Primer semestre de 2019 (Enero-Junio)',
            'position': 'center',
            'width': 700,
            'height':500
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
        tabla();
      }
    </script>
  </head>
  <body>
    
    <div style="width: 700px; margin: auto; ">       
        Año
        <select class="selectpicker" id="anio_ouv" >
          <?php 
                   
            $consulta= "SELECT DISTINCT anio FROM ouvm_tip_mod order by anio desc";
            $resultado=mysqli_query($conexion, $consulta);

            while ($fila=mysqli_fetch_row($resultado)) {
              echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
            }
           
            ?>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Periodo
        <select class="selectpicker" id="periodo_ouv" onchange="consultar();">
          <?php
        
            $consulta= "SELECT DISTINCT semestre FROM ouvm_tip_mod order by semestre desc";
            $resultado=mysqli_query($conexion, $consulta);

            while ($fila=mysqli_fetch_row($resultado)) {
              echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
            }
            //$_SESSION['semestre']=$semestre;
            ?>
        </select>&nbsp;&nbsp;&nbsp;&nbsp; 
        <button type="button" onclick="selecciona()">Consultar</button>
    </div><br>
    
    <div id="columnchart_material" style="width: 700; height: 500; margin: auto; "></div>
    <br>
    <div style="font:'Gill Sans','Gill Sans MT','Trebuchet MS','Segoe UI','sans-serif', 'Arial';">
    <div class="col-lg-2 col-md-12"></div>
    <div class="col-lg-8 col-md-12">
      <div id="tabla"></div>
    </div>   
    <!--
      <h3 style="font-weight: normal;margin: 0 5% ;">Análisis</h3><br>
      <hr style="background-color: #527DA8; height: 0.5px; margin: 0 5% ;"></hr>
      <p align="justify" style="margin: 0 5% ; padding-top: 12px;">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>
    -->
    </div>
    
  </body>
</html>