<html>

<?php

session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass);
mysqli_select_db($conexion, $datab) or die("error en la conexión");


$anio='2018';
$semestre='1';
$valores = array();
$meses = array();
switch ($semestre) {
  case '1':
      for ($i=1; $i <=6 ; $i++) { 
        for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg='$i' AND anio='$anio' AND region='$j'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores, $value[0]);
        }
      }
      $meses=['Enero','Febrero','Marzo','Abril','Mayo','Junio'];
      $_SESSION['valores2']=$valores;
      $_SESSION['meses2']=$meses;
    break;
    case '2':
      for ($i=7; $i <=12 ; $i++) { 
        for ($j=1; $j <=3 ; $j++) { 
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg='$i' AND anio='$anio' AND region='$j'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores, $value[0]);
        }
      }
      $meses=['Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
      $_SESSION['valores2']=$valores;
      $_SESSION['meses2']=$meses;
    break;
}
?>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css"> 
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
      function tabla(){  
        var grafica='v1_g2';
        $.post("graficas/tablas.php",{ grafica : grafica},function(data){
          $("#tabla").html(data);
        }); 
      }


      function selecciona() {
        var indice_a = anio_ouv.selectedIndex;
        var valor_a = anio_ouv.options;
        var indice_b = periodo_ouv.selectedIndex;

        //alert("Index: " + valor_a[indice_a].index + "es" + valor_a[indice_a].text);
        /* Para el año 2020*/
        if(indice_a == 0 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v1_g2_20.php');
        } 
        else if(indice_a == 0 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v1_g2_2_20.php');
        }
        /* Para el año 2019*/
        if(indice_a == 1 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v1_g2_19.php');
        } 
        else if(indice_a == 1 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v1_g2_2_19.php');
        }
        /* Para el año 2018*/
        if(indice_a == 2 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v1_g2_18.php');
        } 
        else if(indice_a == 2 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v1_g2_2_18.php');
        }
        /* Para el año 2017*/
        if(indice_a == 3 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v1_g2.php');
        } 
        else if(indice_a == 3 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v1_g2_2.php');
        }

      }

      var meses = <?php echo json_encode($meses);?> ;
      google.charts.load('current', {'packages':['corechart', 'bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var button = document.getElementById('change-chart');
        var chartDiv = document.getElementById('columnchart_material');

        var data = google.visualization.arrayToDataTable([
          ['Mes', 'Región Norte', 'Región Centro', 'Región Sur'],
          [meses[0], <?php echo $valores[0];?>, <?php echo $valores[1];?>,<?php echo $valores[2];?>],
          [meses[1], <?php echo $valores[3];?>, <?php echo $valores[4];?>,<?php echo $valores[5];?>],
          [meses[2], <?php echo $valores[6];?>, <?php echo $valores[7];?>,<?php echo $valores[8];?>],
          [meses[3], <?php echo $valores[9];?>, <?php echo $valores[10];?>,<?php echo $valores[11];?>],
          [meses[4], <?php echo $valores[12];?>, <?php echo $valores[13];?>,<?php echo $valores[14];?>],
          [meses[5], <?php echo $valores[15];?>, <?php echo $valores[16];?>,<?php echo $valores[17];?>]
        ]);

        var baroptions = {
          chart: {
            title: 'Casos de violencia por mes en cada región',
            subtitle: 'Primer semestre de 2018 (Enero-Junio)',
            
          }
        };

        var areaoptions = {
          title: 'Casos de violencia ocurridos por mes en cada región',
          hAxis: {title: 'Mes',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        function barChart(){
          var barchart = new google.charts.Bar(chartDiv);
          barchart.draw(data, google.charts.Bar.convertOptions(baroptions));
          button.innerText = 'Cambia a área';
          button.onclick = areaChart;
        }

        function areaChart(){
          var areachart = new google.visualization.AreaChart(chartDiv);
          areachart.draw(data, areaoptions);
          button.innerText = 'Cambia a columnas';
          button.onclick = barChart;
        }

        barChart();
        tabla();
      }
    </script>
  </head>
  <body>
    <div style="width: 700px; margin: auto; ">       
        Año
        <select class="selectpicker" id="anio_ouv" >
          <?php 
                   
            $consulta= "SELECT DISTINCT anio FROM ouvm_tip_mod order by anio desc";
            $resultado=mysqli_query($conexion, $consulta);

            while ($fila=mysqli_fetch_row($resultado)) {
              echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
            }
           
            ?>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Periodo
        <select class="selectpicker" id="periodo_ouv" onchange="consultar();">
          <?php
        
            $consulta= "SELECT DISTINCT semestre FROM ouvm_tip_mod order by semestre desc";
            $resultado=mysqli_query($conexion, $consulta);

            while ($fila=mysqli_fetch_row($resultado)) {
              echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
            }
            //$_SESSION['semestre']=$semestre;
            ?>
        </select>&nbsp;&nbsp;&nbsp;&nbsp; 
        <button type="button" onclick="selecciona()">Consultar</button><br><br>
        <button id="change-chart" style="">Cambia a área</button>

    </div><br>
    
    <div id="columnchart_material" style="width: 700px; height: 500px; margin: auto;">
       
    </div>
    <br>
    <div style="font:'Gill Sans','Gill Sans MT','Trebuchet MS','Segoe UI','sans-serif', 'Arial';">
    <div class="col-lg-2 col-md-12"></div>
    <div class="col-lg-8 col-md-12">
      <div id="tabla"></div>
    </div>
    <!--
      <h3 style="font-weight: normal;margin: 0 5% ;">Análisis</h3><br>
      <hr style="background-color: #527DA8; height: 0.5px; margin: 0 5% ;"></hr>
      <p align="justify" style="margin: 0 5% ; padding-top: 12px;">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>
    -->
    </div>
  </body>
</html>