
<?php

session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass);
mysqli_select_db($conexion, $datab) or die("error en la conexión");
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../index.css"/>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link href="../css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <style type="text/css">
    
      #a_v1_g3{
        color: #567AA6;
        font-size: 14px;
        cursor: pointer;
        margin: 10px;
      }

      #a_v1_g3:hover{
        text-decoration: none; 
      }

      #conenido_v1_g3{
        margin: auto;
      }

    </style>

  </head>

  <body>
    
      <div class="container"> 
        <div class="row">
          <div style="width: 700px; margin: auto; ">       
          Año
          <select class="selectpicker" id="anio_ouv" >
            <?php 
                     
              $consulta= "SELECT DISTINCT anio FROM ouvm_tip_mod order by anio desc";
              $resultado=mysqli_query($conexion, $consulta);

              while ($fila=mysqli_fetch_row($resultado)) {
                echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
              }
             
              ?>
          </select>
          &nbsp;&nbsp;&nbsp;&nbsp;
          Periodo
          <select class="selectpicker" id="periodo_ouv" onchange="consultar();">
            <?php
          
              $consulta= "SELECT DISTINCT semestre FROM ouvm_tip_mod order by semestre desc";
              $resultado=mysqli_query($conexion, $consulta);

              while ($fila=mysqli_fetch_row($resultado)) {
                echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
              }
              //$_SESSION['semestre']=$semestre;
              ?>
          </select>&nbsp;&nbsp;&nbsp;&nbsp; 
          <button type="button" onclick="selecciona()">Consultar</button>
          </div><br>
             
          <div id="contenido_v1_g3"><!--CONTENIDO DE LAS GRAFICAS--></div>
          <br><br>

          <div class="col-lg-3 col-sm-12"></div>
          <div id="li_v1_g3" class="col-lg-2 col-sm-12">   
            <li title="Estatal"><a class="a_v1_g3" id="v1_g3_1" href="#">Estatal</a></li>
          </div> 
          <div id="li_v1_g3" class="col-lg-2 col-sm-12"> 
            <li title="Región Norte"><a class="a_v1_g3" id="v1_g3_2" href="#">Región Norte</a></li>
          </div> 
          <div id="li_v1_g3" class="col-lg-2 col-sm-12"> 
            <li title="Región Centro"><a class="a_v1_g3" id="v1_g3_3" href="#">Región Centro</a></li>
          </div> 
          <div id="li_v1_g3" class="col-lg-2 col-sm-12"> 
            <li title="Región Sur"><a class="a_v1_g3" id="v1_g3_4" href="#">Región Sur</a></li>
          </div><br><br>

          <!-- Espacio en blanco-->
          <div class="col-lg-3 col-sm-12"></div>

          <!--Tabla de leyenda de datos -->
          <div class="col-lg-8 col-sm-12">
            <table class="table table-condensed" style=" font-size: 12px; ">
              <tbody>
              <tr>
                <td>VPS= Violencia Psicológica</td>
                <td>VF= Violencia Física</td>
                <td>VS= Violencia Sexual</td>
                <td>VPa= Violencia Patrimonial</td>
                <td>VE= Violencia Económica</td>
                <td>VO= Violencia Obstétrica</td>
              </tr> 
              </tbody> 
            </table>        
		      </div> 
        </div>
      </div>
      <!--Script para cargar las páginas externas php-->
      <script type="text/javascript">
             function selecciona() {
              var indice_a = anio_ouv.selectedIndex;
              var valor_a = anio_ouv.options;
              var indice_b = periodo_ouv.selectedIndex;

              //alert("Index: " + valor_a[indice_a].index + "es" + valor_a[indice_a].text);
              //Julio-Diciembre 2020
              if(indice_a == 0 &&  indice_b == 0){
                indice_b.selectedIndex = 0;
                $('#contenido_v1_g3').load('graficas/v1_g3_20.php');
                 $("#contenido_v1_g3").load('graficas/v1_g3_1_20.php');
                 $('#contenido_v1_g3').load('graficas/v1_g3_2_20.php');
                 $('#contenido_v1_g3').load('graficas/v1_g3_3_20.php');
                 $('#contenido_v1_g3').load('graficas/v1_g3_4_20.php');

              } 
              //Enero-Junio 2020
              else if(indice_a == 0 && indice_b == 1){
                indice_b.selectedIndex = 1;
                //$('#contenido_v1_g3').load('graficas/v1_g3-2_19.php');
                //$("#contenido_v1_g3").load('graficas/v1_g3_1-2_19.php');
                //$('#contenido_v1_g3').load('graficas/v1_g3_2-2_19.php');
                //$('#contenido_v1_g3').load('graficas/v1_g3_3-2_19.php');
                //$('#contenido_v1_g3').load('graficas/v1_g3_4-2_19.php');
              }

              //Enero-Junio 2019
              if(indice_a == 1 &&  indice_b == 0){
                indice_b.selectedIndex = 0;
                $('#contenido_v1_g3').load('graficas/v1_g3_19.php');
                $("#contenido_v1_g3").load('graficas/v1_g3_1_19.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_2_19.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_3_19.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_4_19.php');
                
              } 
              //Julio-Diciembre 2019
              else if(indice_a == 1 && indice_b == 1){
                indice_b.selectedIndex = 1;
                $('#contenido_v1_g3').load('graficas/v1_g3-2_19.php');
                $("#contenido_v1_g3").load('graficas/v1_g3_1-2_19.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_2-2_19.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_3-2_19.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_4-2_19.php');
              }


              //Enero-Junio 2018
              if(indice_a == 2 &&  indice_b == 0){
                indice_b.selectedIndex = 0;
                $('#contenido_v1_g3').load('graficas/v1_g3_18.php');
                $("#contenido_v1_g3").load('graficas/v1_g3_1_18.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_2_18.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_3_18.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_4_18.php');
              } 
              //Julio-Diciembre 2018
              else if(indice_a == 2 && indice_b == 1){
                indice_b.selectedIndex = 1;
                $('#contenido_v1_g3').load('graficas/v1_g3-2_18.php');
                $("#contenido_v1_g3").load('graficas/v1_g3_1-2_18.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_2-2_18.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_3-2_18.php');
                $('#contenido_v1_g3').load('graficas/v1_g3_4-2_18.php');
              }

              //Enero-Junio 2017
              if(indice_a == 3 &&  indice_b == 0){
                indice_b.selectedIndex = 0;
                 $('#contenido_v1_g3').load('graficas/v1_g3-2.php');
                 $("#contenido_v1_g3").load('graficas/v1_g3_1.php');
                 $('#contenido_v1_g3').load('graficas/v1_g3_2.php');
                 $('#contenido_v1_g3').load('graficas/v1_g3_3.php');
                 $('#contenido_v1_g3').load('graficas/v1_g3_4.php');
                
              }
              //Julio-Diciembre 2017
              else if(indice_a == 3 && indice_b == 1){
                indice_b.selectedIndex = 1;
                $('#contenido_v1_g3').load('graficas/v1_g3.php'); //Página actual para cargar las demas
                $("#contenido_v1_g3").load('graficas/v1_g3_1-2.php'); //estado de Veracruz
                $('#contenido_v1_g3').load('graficas/v1_g3_2-2.php'); //Region Norte
                $('#contenido_v1_g3').load('graficas/v1_g3_3-2.php'); //Region Centro
                $('#contenido_v1_g3').load('graficas/v1_g3_4-2.php'); //Region sur
              }

            }

      $(document).ready(function(){
        $("#contenido_v1_g3").load('graficas/v1_g3_1-2.php');
      });
      </script>
     
</body>     