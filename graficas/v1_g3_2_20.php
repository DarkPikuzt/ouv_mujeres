<html>

<?php

session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass);
mysqli_select_db($conexion, $datab) or die("error en la conexión");


$anio='2020';
$semestre='2';

switch ($semestre){
  case '1':
      $query="SELECT t_violencia,COUNT(*) AS COUNT FROM ouvm_tip_mod WHERE mes_reg<=6 AND anio='$anio' GROUP BY t_violencia ORDER BY COUNT DESC";
       $rquery=mysqli_query($conexion, $query) or die();
        
    break;
    case '2':
      $query="SELECT t_violencia,COUNT(*) AS COUNT FROM ouvm_tip_mod WHERE mes_reg>=7 AND anio='$anio' GROUP BY t_violencia ORDER BY COUNT DESC";
       $rquery=mysqli_query($conexion, $query) or die();
    break;
}
?>
  <head>
   <meta charset="UTF-8">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Tipos de violencia', 'Número de casos'],
          <?php while($row=mysqli_fetch_row($rquery)){ 
          echo "['".$row[0]."',".$row[1]."],";
          } ?>
        ]);

        var options = {
          chart: {
            title: 'Combinaciones de tipos de violencia - Región Norte',
            subtitle: 'Segundo semestre de 2020 (Frecuencia acumulada al mes de Septiembre)',
            position: 'center',
          },
          bars: 'horizontal',
          legend: {position: 'none'}
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
  </head>
  <body>
    
    <div id="columnchart_material" style="width: 700px; height: 500px; margin: auto;"></div>
     <br><br>
    <div class="col-lg-3 col-sm-12"></div>
    <div class="col-lg-8 col-sm-12">
            <table class="table table-condensed" style=" font-size: 12px; ">
              <tbody>
              <tr>
                <td>VPS= Violencia Psicológica</td>
                <td>VF= Violencia Física</td>
                <td>VS= Violencia Sexual</td>
                <td>VPa= Violencia Patrimonial</td>
                <td>VE= Violencia Económica</td>
                <td>VO= Violencia Obstétrica</td>
              </tr> 
              </tbody> 
            </table>        
          </div> 
  </body>
</html>