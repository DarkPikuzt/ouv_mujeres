<html>

<?php

session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass);
mysqli_select_db($conexion, $datab) or die("error en la conexión");


$anio='2017';
$semestre='2';

switch ($semestre){
  case '1':
      $query="SELECT t_violencia,COUNT(*) AS COUNT FROM ouvm_tip_mod WHERE mes_reg<=6 AND anio='$anio' AND region='3' GROUP BY t_violencia ORDER BY COUNT DESC";
       $rquery=mysqli_query($conexion, $query) or die();
        
    break;
    case '2':
      $query="SELECT t_violencia,COUNT(*) AS COUNT FROM ouvm_tip_mod WHERE mes_reg>=7 AND anio='$anio' AND region='3' GROUP BY t_violencia ORDER BY COUNT DESC";
       $rquery=mysqli_query($conexion, $query) or die();
    break;
}
?>
  <head>
   <meta charset="UTF-8">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Tipos de violencia', 'Número de casos'],
          <?php while($row=mysqli_fetch_row($rquery)){ 
          echo "['".$row[0]."',".$row[1]."],";
          } ?>
        ]);

        var options = {
          chart: {
            title: 'Combinaciones de tipos de violencia - Región Sur',
            subtitle: 'Segundo semestre de 2017 (Julio-Diciembre)',
            position: 'center',
          },
          bars: 'horizontal',
          legend: {position: 'none'}
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
  </head>
  <body>
    
    <div id="columnchart_material" style="width: 700px; height: 500px; margin: auto;"></div>
    
  </body>
</html>