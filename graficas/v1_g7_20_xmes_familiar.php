<html>

<?php

session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass);
mysqli_select_db($conexion, $datab) or die("error en la conexión");


$anio='2020';
$semestre='1';
$valores = array();
$meses = array();
switch ($semestre) {
  case '1':
      for ($i=1; $i <=6 ; $i++) { 
         
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg='$i' AND anio='$anio' AND modalidad='Familiar'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores, $value[0]);
  
      }
      $meses=['Enero','Febrero','Marzo','Abril','Mayo','Junio'];
      $_SESSION['valores2']=$valores;
      $_SESSION['meses2']=$meses;
    break;
    case '2':
      for ($i=7; $i <=12 ; $i++) { 
       
          $query="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg='$i' AND anio='$anio' AND modalidad='Familiar'";
          $rquery=mysqli_query($conexion, $query) or die();
          $value = mysqli_fetch_array($rquery);
          array_push($valores, $value[0]);
      
      }
      $meses=['Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
      $_SESSION['valores2']=$valores;
      $_SESSION['meses2']=$meses;
    break;
}
?>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css"> 
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
     

     
      var meses = <?php echo json_encode($meses);?> ;
      google.charts.load('current', {'packages':['corechart', 'bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var button = document.getElementById('change-chart');
        var chartDiv = document.getElementById('columnchart_material');

        var data = google.visualization.arrayToDataTable([
          ['Mes', 'Violencia Familiar o doméstica'],
          [meses[0], <?php echo $valores[0];?>],
          [meses[1], <?php echo $valores[1];?>],
          [meses[2], <?php echo $valores[2];?>],
          [meses[3], <?php echo $valores[3];?>],
          [meses[4], <?php echo $valores[4];?>],
          [meses[5], <?php echo $valores[5];?>]
        ]);

        var baroptions = {
          chart: {
            title: 'Casos de violencia familiar o doméstica por mes',
            subtitle: 'Enero a Junio de 2020',
            
          }
        };

        var areaoptions = {
          title: 'Casos de violencia familiar o doméstica por mes',
          hAxis: {title: 'Mes',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        function barChart(){
          var barchart = new google.charts.Bar(chartDiv);
          barchart.draw(data, google.charts.Bar.convertOptions(baroptions));
          button.innerText = 'Cambia a área';
          button.onclick = areaChart;
        }

        function areaChart(){
          var areachart = new google.visualization.AreaChart(chartDiv);
          areachart.draw(data, areaoptions);
          button.innerText = 'Cambia a columnas';
          button.onclick = barChart;
        }

        barChart();
        tabla();
      }
    </script>
  </head>
  <body>
    
    </div>
    <div class="col-lg-12 " style="width: 92%;"><button id="change-chart" style="">Cambia a área</button></div>
    <br>
    <div id="columnchart_material" style="width: 97%; height: 300px; margin: auto;">
       
    </div>
    
   
    </div>
  </body>
</html>