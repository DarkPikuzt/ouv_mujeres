<html>

<?php

session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass); 
mysqli_select_db($conexion, $datab) or die("error en la conexión");


$anio='2020';
$semestre='2';
$_SESSION['anio']=$anio;
switch ($semestre) {
  case '1':
      $queryh="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg<=6 AND anio='$anio' AND modalidad='Familiar' ";
      $rqueryh=mysqli_query($conexion, $queryh) or die();
      $valueh = mysqli_fetch_array($rqueryh);
      $familiar=$valueh[0];

      $querym="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg<=6 AND anio='$anio' AND modalidad='Comunidad'";
      $rquerym=mysqli_query($conexion, $querym) or die();
      $valuem = mysqli_fetch_array($rquerym);
      $comunidad=$valuem[0];

      $queryg="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg<=6 AND anio='$anio' AND modalidad='Escolar'";
      $rqueryg=mysqli_query($conexion, $queryg) or die();
      $valueg = mysqli_fetch_array($rqueryg);
      $escolar=$valueg[0];

      $queryn="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg<=6 AND anio='$anio' AND modalidad='Institucional'";
      $rqueryn=mysqli_query($conexion, $queryn) or die();
      $valuen = mysqli_fetch_array($rqueryn);
      $institucional=$valuen[0];
    
      $queryg="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg<=6 AND anio='$anio' AND modalidad='Feminicida'";
      $rqueryg=mysqli_query($conexion, $queryg) or die();
      $valuef = mysqli_fetch_array($rqueryg);
      $feminicida=$valuef[0];

      $queryn="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg<=6 AND anio='$anio' AND modalidad='Género'";
      $rqueryn=mysqli_query($conexion, $queryn) or die();
      $valuegen = mysqli_fetch_array($rqueryn);
      $genero=$valuegen[0];

      $queryn="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg<=6 AND anio='$anio' AND modalidad='No se especifica'";
      $rqueryn=mysqli_query($conexion, $queryn) or die();
      $valuenes = mysqli_fetch_array($rqueryn);
      $noesp=$valuenes[0];
   
   
    break;
    case '2':
       $queryh="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg>=7 AND anio='$anio' AND modalidad='Familiar'";
      $rqueryh=mysqli_query($conexion, $queryh) or die();
      $valueh = mysqli_fetch_array($rqueryh);
      $familiar=$valueh[0];

      $querym="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg>=7 AND anio='$anio' AND modalidad='Comunidad'";
      $rquerym=mysqli_query($conexion, $querym) or die();
      $valuem = mysqli_fetch_array($rquerym);
      $comunidad=$valuem[0];

      $queryg="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg>=7 AND anio='$anio' AND modalidad='Escolar'";
      $rqueryg=mysqli_query($conexion, $queryg) or die();
      $valueg = mysqli_fetch_array($rqueryg);
      $escolar=$valueg[0];

      $queryn="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg>=7 AND anio='$anio' AND modalidad='Institucional'";
      $rqueryn=mysqli_query($conexion, $queryn) or die();
      $valuen = mysqli_fetch_array($rqueryn);
      $institucional=$valuen[0];
    
      $queryg="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg>=7 AND anio='$anio' AND modalidad='Feminicida'";
      $rqueryg=mysqli_query($conexion, $queryg) or die();
      $valuef = mysqli_fetch_array($rqueryg);
      $feminicida=$valuef[0];

      $queryn="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg>=7 AND anio='$anio' AND modalidad='Género'";
      $rqueryn=mysqli_query($conexion, $queryn) or die();
      $valuegen = mysqli_fetch_array($rqueryn);
      $genero=$valuegen[0];

      $queryn="SELECT COUNT(id) FROM ouvm_tip_mod WHERE mes_reg>=7 AND anio='$anio' AND modalidad='No se especifica'";
      $rqueryn=mysqli_query($conexion, $queryn) or die();
      $valuenes = mysqli_fetch_array($rqueryn);
      $noesp=$valuenes[0];
    break;
}
?>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css"> 
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">

      function tabla(){ 
        var grafica='v1_g7-2';
        $.post("graficas/tablas.php",{ grafica : grafica },function(data){
          $("#tabla").html(data);
        }); 
      }

       /*  $(document).ready(function(){

       var semestre='2';
        $.post("graficas/v1_g7_2_20_tipos_familiar.php",{ semestre : semestre },function(data){
          $("#tipo_fam").html(data);
        });

        var semestre2='2';
        $.post("graficas/v1_g7_2_20_xmes_familiar.php",{ semestre2 : semestre2 },function(data){
          $("#mes_fam").html(data);
        });

         });*/
         //$('#columnchart_material2').append('graficas/v1_g7_2_20_xmes_familiar.php');
          //$('#columnchart_material2').append($('<div>').load('graficas/v1_g7_2_20_xmes_familiar.php'));
      

      function selecciona() {
        var indice_a = anio_ouv.selectedIndex;
        var valor_a = anio_ouv.options;
        var indice_b = periodo_ouv.selectedIndex;

        //alert("Index: " + valor_a[indice_a].index + "es" + valor_a[indice_a].text);
        //Valores para el año 2020
        if(indice_a == 0 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v1_g7_20.php');
         
          //alert("periodo documento 2-1: " + indice_b);
        } 
       else if(indice_a == 0 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v1_g7_2_20.php');
          
        }
  
      }

      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Modialidad de la violencia', 'Número de casos'],
          ['Familiar o doméstica',<?php echo $familiar?>],            // RGB value
          ['Comunidad',<?php echo $comunidad?>],            // English color name
          ['Escolar',<?php echo $escolar?>],
          ['Institucional',<?php echo $institucional?>], // CSS-style declaration
          ['Feminicida',<?php echo $feminicida?>],            // English color name
          ['Género',<?php echo $genero?>],
          ['No se especifica',<?php echo $noesp?>], // CSS-style declaration
        ]);

        var options = {
          chart: {
            title: 'Modolidad de la violencia',
            subtitle: 'Segundo semestre de 2020 (Frecuencia acumulada al mes de Septiembre)',
            position: 'center',
          },
          bar: {groupWidth: '30%'},
          legend: {position: 'none'}
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
        tabla();
        
      }

  
  </script>
  </head>
  <body>
    <div style="width: 700px; margin: auto; ">       
        Año
        <select class="selectpicker" id="anio_ouv" >
          <?php 
                   
            $consulta= "SELECT DISTINCT anio FROM ouvm_tip_mod order by anio desc";
            $resultado=mysqli_query($conexion, $consulta);

            while ($fila=mysqli_fetch_row($resultado)) {
              echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
            }

            ?>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Periodo
        <select class="selectpicker" id="periodo_ouv" onchange="consultar();">
          <?php
        
            $consulta= "SELECT DISTINCT semestre FROM ouvm_tip_mod order by semestre desc";
            $resultado=mysqli_query($conexion, $consulta);

            while ($fila=mysqli_fetch_row($resultado)) {
              echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
            }
            //$_SESSION['semestre']=$semestre;
            ?>
        </select>&nbsp;&nbsp;&nbsp;&nbsp; 
        <button type="button" onclick="selecciona()">Consultar</button>
    </div><br>
    
    <div id="columnchart_material" style="width: 700px; height: 400px; margin: auto;"></div>
    <div><br>
    
      
    <div style="font:'Gill Sans','Gill Sans MT','Trebuchet MS','Segoe UI','sans-serif', 'Arial';">
    <div class="col-lg-2 col-md-12"></div>
    <div class="col-lg-8 col-md-12">
      
      <div id="tabla"></div>

      <br><br>
      <!--<div id="mes_fam"></div>
      <div id="tipo_fam" style="width: 700px; height: 400px; margin: auto;"></div>-->
      <div class="" style="width: 100%; color: #ccccc; background-color: #ccc;height: 1px; padding-top: .2px; "><h5 ></h5></div>
    <br>
      <?php
              
        echo "<iframe src='graficas/v1_g7_2_20_xmes_familiar.php' width='100%' height='380px' frameborder='0' framespacing='0' scrolling='auto' border='0'></iframe>";
       echo "<iframe src='graficas/v1_g7_2_20_tipos_familiar.php' width='100%' height='400px' frameborder='0' framespacing='0' scrolling='auto' border='0'></iframe>";   
        
    
      ?>

    </div>

       
    </div>
  </body>
</html>