<html>

<?php

session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass); 
mysqli_select_db($conexion, $datab) or die("error en la conexión");


$anio='2020';
$semestre='2';
$_SESSION['anio']=$anio;

//$semestre=$_POST['semestre'];

switch ($semestre) {
  case '1':

      /* combinaciones de tipo de violencia para modalidad "Familiar"*/
      $query="SELECT  t_violencia,COUNT(*) AS COUNT FROM ouvm_tip_mod WHERE mes_reg<=6 AND anio='2020' AND modalidad='Familiar' GROUP BY t_violencia ORDER BY COUNT(t_violencia) DESC";
       $rquery=mysqli_query($conexion, $query) or die();
    
  
    break;
    case '2':
      $query="SELECT  t_violencia,COUNT(*) AS COUNT FROM ouvm_tip_mod WHERE mes_reg>=7 AND anio='2020' AND modalidad='Familiar' GROUP BY t_violencia ORDER BY COUNT(t_violencia) DESC";
       $rquery=mysqli_query($conexion, $query) or die();
    
       
    break;
}
?>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css"> 
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">

     
      <!-- //Para la gráfica de conteo de violencias por modalidad familiar-->
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Tipos de violencia', 'Número de casos'],
          <?php while($row=mysqli_fetch_row($rquery)){ 
          echo "['".$row[0]."',".$row[1]."],";
          } ?>
        ]);

        var options = {
          chart: {
            title: 'Combinaciones de tipos de violencia familiar o doméstica ',
            subtitle: 'Segundo semestre de 2020 (Frecuencia acumulada al mes de Septiembre)',
            position: 'center',
          },
          bars: 'horizontal',
          legend: {position: 'none'}
        };

        var chart1 = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart1.draw(data, google.charts.Bar.convertOptions(options));

        
      }



    </script>
  </head>
  <body>
    <div id="columnchart_material" style="width: 92%; height: 280px; margin: auto;"> </div>
    <br><br>
    <div class="col-lg-3 col-sm-12"></div>
    <div class="col-lg-8 col-sm-12">
            <table class="table table-condensed" style=" font-size: 12px; width: 85%;  margin: 0 120px; ">
              <tbody>
              <tr>
                <td>VPS= Violencia Psicológica</td>
                <td>VF= Violencia Física</td>
                <td>VS= Violencia Sexual</td>
                <td>VPa= Violencia Patrimonial</td>
                <td>VE= Violencia Económica</td>
                <td>VO= Violencia Obstétrica</td>
              </tr> 
              </tbody> 
            </table>        
          </div> 

  </body>
</html>