<html>
<?php

session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass);
mysqli_select_db($conexion, $datab) or die("error en la conexión");


$anio='2020';
$semestre='2';
$_SESSION['anio']=$anio;
switch ($semestre){
  case '1':
      $query1="SELECT COUNT(id) FROM ouvm_desap WHERE mes_reg<=6 AND anio='$anio' AND localiza='0'";
      $rquery1=mysqli_query($conexion, $query1) or die();
      $reg1 = mysqli_fetch_array($rquery1);

      $query2="SELECT COUNT(id) FROM ouvm_desap WHERE mes_reg<=6 AND anio='$anio' AND localiza='1'";
      $rquery2=mysqli_query($conexion, $query2) or die();
      $reg2 = mysqli_fetch_array($rquery2);

    break;
    case '2':
      $query1="SELECT COUNT(id) FROM ouvm_desap WHERE mes_reg>=7 AND anio='$anio' AND localiza='0'";
      $rquery1=mysqli_query($conexion, $query1) or die();
      $reg1 = mysqli_fetch_array($rquery1);

      $query2="SELECT COUNT(id) FROM ouvm_desap WHERE mes_reg>=7 AND anio='$anio' AND localiza='1'";
      $rquery2=mysqli_query($conexion, $query2) or die();
      $reg2 = mysqli_fetch_array($rquery2);

    break;
}
?>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css"> 
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">

      function tabla(){ 
        var grafica='v2_g4-2';
        $.post("graficas/tablas.php",{ grafica : grafica },function(data){
          $("#tabla").html(data);
        }); 
      }

      function selecciona() {
        var indice_a = anio_ouv.selectedIndex;
        var valor_a = anio_ouv.options;
        var indice_b = periodo_ouv.selectedIndex;

        //alert("Index: " + valor_a[indice_a].index + "es" + valor_a[indice_a].text);
        //Valores para 2020
        if(indice_a == 0 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#piechart').load('graficas/v2_g4_20.php');
        } 
        else if(indice_a == 0 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#piechart').load('graficas/v2_g4_2_20.php');
        }
        //Valores para 2019
        if(indice_a == 1 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#piechart').load('graficas/v2_g4_19.php');
        } 
        else if(indice_a == 1 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#piechart').load('graficas/v2_g4_2_19.php');
        }
        //Valores para 2018
        if(indice_a == 2 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#piechart').load('graficas/v2_g4_18.php');
        } 
        else if(indice_a == 2 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#piechart').load('graficas/v2_g4_2_18.php');
        }
        //Valores para 2017
        if(indice_a == 3 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#piechart').load('graficas/v2_g4.php');
        } 
        else if(indice_a == 3 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#piechart').load('graficas/v2_g4_2.php');
        }
      }


      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Localización', 'Cantidad'],
          ['Localizadas', <?php echo $reg2[0]?>],
          ['No localizadas', <?php echo $reg1[0]?>]
        ]);

        var options = {
          title: 'Localización de la víctima, segundo semestre de 2020 (Frecuencia acumulada al mes de Septiembre)',
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
        tabla();
      }
    </script>
  </head>
  <body>
    <div style="width: 700px; margin: auto; ">       
        Año
        <select class="selectpicker" id="anio_ouv" >
          <?php 
                   
            $consulta= "SELECT DISTINCT anio FROM ouvm_desap order by anio desc";
            $resultado=mysqli_query($conexion, $consulta);

            while ($fila=mysqli_fetch_row($resultado)) {
              echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
            }
           
            ?>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Periodo
        <select class="selectpicker" id="periodo_ouv" onchange="consultar();">
          <?php
        
            $consulta= "SELECT DISTINCT semestre FROM ouvm_desap order by semestre desc";
            $resultado=mysqli_query($conexion, $consulta);

            while ($fila=mysqli_fetch_row($resultado)) {
              echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
            }
            //$_SESSION['semestre']=$semestre;
            ?>
        </select>&nbsp;&nbsp;&nbsp;&nbsp; 
        <button type="button" onclick="selecciona()">Consultar</button>
    </div><br>

    <div id="piechart" style="width: 700px; height: 500px; margin: auto;"></div>
    <br>
    <div style="font:'Gill Sans','Gill Sans MT','Trebuchet MS','Segoe UI','sans-serif', 'Arial';">
    <div class="col-lg-2 col-md-12"></div>
    <div class="col-lg-8 col-md-12">
      <div id="tabla"></div>
    </div>
    <!--
      <h3 style="font-weight: normal;margin: 0 5% ;">Análisis</h3><br>
      <hr style="background-color: #527DA8; height: 0.5px; margin: 0 5% ;"></hr>
      <p align="justify" style="margin: 0 5% ; padding-top: 12px;">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>
    -->
    </div>
  </body>
</html>