<html>

<?php

session_start();
include "../conexion.php";
$conexion = mysqli_connect($host,$usuario,$pass);
mysqli_select_db($conexion, $datab) or die("error en la conexión");
mysqli_set_charset($conexion,"utf8");

$anio='2020';
$semestre='2';

switch ($semestre){
  case '1':
      $query="SELECT causa_muerte,COUNT(*) AS COUNT FROM ouvm_homici WHERE mes_reg<=6 AND anio='$anio' GROUP BY causa_muerte ORDER BY causa_muerte ASC";
       $rquery=mysqli_query($conexion, $query) or die();
        
    break;
  case '2':
     $query="SELECT causa_muerte,COUNT(*) AS COUNT FROM ouvm_homici WHERE mes_reg>=7 AND anio='$anio' GROUP BY causa_muerte ORDER BY causa_muerte ASC";
       $rquery=mysqli_query($conexion, $query) or die();
    break;
}
?>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.css"> 
    <meta charset="UTF-8">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      function selecciona() {
        var indice_a = anio_ouv.selectedIndex;
        var valor_a = anio_ouv.options;
        var indice_b = periodo_ouv.selectedIndex;

        //alert("Index: " + valor_a[indice_a].index + "es" + valor_a[indice_a].text);
       //Valores para 2020
        if(indice_a == 0 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v4_g5_20.php');
        } 
        else if(indice_a == 0 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v4_g5_2_20.php');
        }
        //Valores para 2019
        if(indice_a == 1 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v4_g5_19.php');
        } 
        else if(indice_a == 1 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v4_g5_2_19.php');
        }
       //Valores para 2018
        if(indice_a == 2 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v4_g5_18.php');
        } 
        else if(indice_a == 2 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v4_g5_2_18.php');
        }
        //Valores para 2017
        if(indice_a == 3 &&  indice_b == 1){
          indice_b.selectedIndex = 1;
          $('#columnchart_material').load('graficas/v4_g5.php');
        } 
        else if(indice_a == 3 && indice_b == 0){
          indice_b.selectedIndex = 0;
          $('#columnchart_material').load('graficas/v4_g5_2.php');
        }
      }

      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Causas de la muerte', 'Número de casos'],
          <?php while($row=mysqli_fetch_row($rquery)){ 
          echo "['".$row[0]."',".$row[1]."],";
          } ?>
        ]);

        var options = {
          chart: {
            title: 'Causas de la muerte',
            subtitle: 'Segundo semestre de 2020 (Frecuencia acumulada al mes de Septiembre)',
            position: 'center',
          },
          //bars: 'horizontal',
          bar: {groupWidth: '30%'},
          legend: {position: 'none'}
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
  </head>
  <body>
    <div style="width: 700px; margin: auto; ">       
        Año
        <select class="selectpicker" id="anio_ouv" >
          <?php 
                   
            $consulta= "SELECT DISTINCT anio FROM ouvm_homici order by anio desc";
            $resultado=mysqli_query($conexion, $consulta);

            while ($fila=mysqli_fetch_row($resultado)) {
              echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
            }

            ?>
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Periodo
        <select class="selectpicker" id="periodo_ouv" onchange="consultar();">
          <?php
        
            $consulta= "SELECT DISTINCT semestre FROM ouvm_homici order by semestre desc";
            $resultado=mysqli_query($conexion, $consulta);

            while ($fila=mysqli_fetch_row($resultado)) {
              echo "<option value='".$fila[0]."'>".$fila[0]."</option>";
            }
            //$_SESSION['semestre']=$semestre;
            ?>
        </select>&nbsp;&nbsp;&nbsp;&nbsp; 
        <button type="button" onclick="selecciona()">Consultar</button>
    </div><br>
    <div id="columnchart_material" style="width: 700px; height: 500px; margin: auto;"></div>
    <br>
    <div>
    <!--
      <h3 style="font-weight: normal;margin: 0 5% ;">Análisis</h3><br>
      <hr style="background-color: #527DA8; height: 0.5px; margin: 0 5% ;"></hr>
      <p align="justify" style="margin: 0 5% ; padding-top: 12px;">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.</p>
    -->
    </div>
  </body>
</html>