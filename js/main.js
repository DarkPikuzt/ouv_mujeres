/*Mandamos a mostra en mapa con los poligonos*/

let dato;
let nortev;

const municipios = "js/municipios/convert.json";
let data;
let acum = 0;

let veracruz = {
    "parte": 0,
    "activo": false,
    "valor":0,
    "TotalZona":0
}

let data2;
let options;
let chart;

const styleMap = [
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
]

function initMap() {

    const styledMapType = new google.maps.StyledMapType(
        [
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "administrative.neighborhood",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            }
        ]
    );
    const fenway = {
        lat: 19.53124,
        lng: -96.91589
    };
    //MAP PRINCIPAR
    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 19.53130,
            lng: -96.91900
        },
        zoom: 6,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: styleMap,

        mapTypeControlOptions: {
            mapTypeIds: ['roadmap', 'satellite', 'styled_map', 'terrain', 'silver_map', 'oscuro_map']
        },
    });

    todos(map);
}
$(document).on('click', '#norte', function() {
    let valor = veracruz["valor"];
    let cad = 'Total del mes de Julio: '


    if (data === undefined) {
        norte(map, valor);
    } else {
        data.setMap(null);
        norte(map, valor);
    }
});

$(document).on('click', '#centro', function() {
    let valor = veracruz["valor"];

    if (data === undefined) {
        centro(map,valor);
    } else {
        data.setMap(null);
        centro(map);
    }
});

$(document).on('click', '#sur', function() {
    let valor = veracruz["valor"];
    //alert(valor)

    if (data === undefined) {
        sur(map,valor);
    } else {
        data.setMap(null);
        sur(map);
    }
});



function selectHandler() {
    let dato = chart.getSelection();
    for (let i = 0; i < dato.length; i++) {
        let item = dato[i];
        let valor = data2.getFormattedValue(item.row, item.column);
        //console.log(item.row);
        if (item.row == 0) {
            veracruz["parte"] = 'Norte';
            veracruz["activo"] = true;
            veracruz["valor"] = valor;
            var thisInput = $(this).find("input[type=radio]");
            var checked = thisInput.is(":checked");
            $("#norte").click();


            console.log(checked)
        }

        if (item.row == 1) {
            veracruz["parte"] = 'Centro';
            veracruz["activo"] = true;
            veracruz["valor"] = valor;
            var thisInput = $(this).find("input[type=radio]");
            var checked = thisInput.is(":checked");
            $("#centro").click();
        //console.log(veracruz["parte"]);
        }

        if (item.row == 2) {
            veracruz["parte"] = 'Sur';
            veracruz["activo"] = true;
            veracruz["valor"] = valor;
            var thisInput = $(this).find("input[type=radio]");
            var checked = thisInput.is(":checked");

            $("#sur").click();
        }


    }
}


    /*funciones para mostrara veracruz pintado*/
    function norte(map,valor) {
        data = new google.maps.Data();
        data.loadGeoJson(municipios);
        data.setStyle({
            strokeColor: "gba(255, 255, 255, 1)",
            strokeOpacity: 0.8,
            strokeWeight: 0.5,
            fillColor: "rgba(0, 0, 0, 0.6)",
            fillOpacity: 0.5
        });
        data.setMap(map);
        MunicipiosNorte(valor);
        verNorte();
    }

    function MunicipiosNorte(valor) {
        let info_window;

        data.addListener('click', function(event) {
            data.revertStyle();
            data.overrideStyle(
                event.feature, {
                    strokeWeight: 2,
                    fillColor: 'rgba(323, 161, 27, 0.75)'
                });

            if (info_window) {
                info_window.setMap(null);
                info_window = null;
            }



            if ( veracruz["activo"] == false){
                contenidos = 'Total Región Norte: '+ veracruz["TotalZona"];
                let contenido  =
                    '<div id="content">' +
                    '<div id="siteNotice">' +
                    contenidos
                '</div>' +
                '</div>';

                info_window = new google.maps.InfoWindow({
                    content: contenido ,
                    position: event.latLng,
                    map: map
                });

            }else{
                contenidos = 'Total zona Norte: '+ veracruz["valor"];
                let contenido  =
                    '<div id="content">' +
                    '<div id="siteNotice">' +
                    contenidos
                '</div>' +
                '</div>';

                info_window = new google.maps.InfoWindow({
                    content: contenido ,
                    position: event.latLng,
                    map: map
                });


            }




            /*

            info_window = new google.maps.InfoWindow({
                content: event.feature.getProperty("NOMGEO"),
                position: event.latLng,
                map: map
            });

             */


            let arrayNorte = [
                13, 27, 33, 34, 35, 37, 40, 50, 51, 55, 56, 58, 60,
                63, 64, 66, 67, 69, 72, 76, 78, 83, 103, 121,
                123, 124, 129, 131, 133, 150, 151, 152, 153, 155,
                157, 158, 154, 160, 161, 167, , 170, 175, 180, 189, 198, 202, 203,
                205
            ]

            data.forEach(function(feature) {
                for (let i = 0; i < arrayNorte.length; i++) {
                    if (feature.getProperty("CVE_MUN") == arrayNorte[i]) {
                        data.overrideStyle(
                            feature, {
                                strokeWeight: 1,
                                fillColor: 'rgba(323, 161, 27, 0.75)'
                            });
                    }
                };
            });

        });


    }

    function verNorte() {
        let info_window;
        data.addListener("addfeature", function(event) {
            data.revertStyle();
            data.overrideStyle(
                event.feature, {
                    strokeWeight: 2,
                    fillColor: 'rgba(32, 161, 27, 0.75)'
                });


            let arrayNorte = [
                13, 27, 33, 34, 35, 37, 40, 50, 51, 55, 56, 58, 60,
                63, 64, 66, 67, 69, 72, 76, 78, 83, 103, 121,
                123, 124, 129, 131, 133, 150, 151, 152, 153, 155,
                157, 158, 154, 160, 161, 167, , 170, 175, 180, 189, 198, 202, 203,
                205
            ]

            data.forEach(function(feature) {
                for (let i = 0; i < arrayNorte.length; i++) {
                    if (feature.getProperty("CVE_MUN") == arrayNorte[i]) {
                        data.overrideStyle(
                            feature, {
                                strokeWeight: 1,
                                fillColor: 'rgba(323, 161, 27, 0.75)'
                            });
                    }
                };
            });
        }, true);
    }



    /* veracruz centro  */
    function centro(map,valor) {
        data = new google.maps.Data();
        data.loadGeoJson(municipios);
        data.setStyle({
            strokeColor: "rgba(0, 0, 0, 0.91)",
            strokeOpacity: 0.8,
            strokeWeight: 0.5,
            fillColor: "rgba(122, 187, 240, 0.7)",
            fillOpacity: 0.5
        });
        data.setMap(map);
        MunicipiosCentro(valor);
        verCentro();
    }

    function MunicipiosCentro(valor) {
        let info_window;
        data.addListener('click', function(event) {
            data.revertStyle();
            data.overrideStyle(
                event.feature, {
                    strokeWeight: 2,
                    fillColor: 'rgba(32, 161, 27, 0.75)'
                });

            if (info_window) {
                info_window.setMap(null);
                info_window = null;
            }



            if ( veracruz["activo"] == false){
                contenidos = 'Total Región centro: '+ veracruz["TotalZona"];
                let contenido  =
                    '<div id="content">' +
                    '<div id="siteNotice">' +
                    contenidos
                '</div>' +
                '</div>';

                info_window = new google.maps.InfoWindow({
                    content: contenido ,
                    position: event.latLng,
                    map: map
                });

            }else{
                contenidos = 'Total zona Centro: '+ veracruz["valor"];
                let contenido  =
                    '<div id="content">' +
                    '<div id="siteNotice">' +
                    contenidos
                '</div>' +
                '</div>';

                info_window = new google.maps.InfoWindow({
                    content: contenido ,
                    position: event.latLng,
                    map: map
                });


            }




            /*

            info_window = new google.maps.InfoWindow({
                content: event.feature.getProperty("NOMGEO"),
                position: event.latLng,
                map: map
            });

             */


            let arrayCentro = [
                1, 2, 4, 6, 7, 8, 9, 10, 14, 16, 17, 18,
                19, 20, 21, 22, 23, 24, 25, 26, 28, 29, 30, 31,
                36, 38, 41, 42, 43, 44, 46, , 47, 49, 52, 53, 57,
                62, 65, 68, 71, 74, 79, 80, 81, 85, 86,
                87, 88, 90, 92, 93, 95, 96, 98, 99, 100, 101,
                102, 105, 106, 107, 109, 110, 112, 113, 114,
                115, 117, 118, 125, 126, 127, 128,
                132, 134, 135, 136, 137, 138, 140, 146,
                147, 148, 156, 159, 162, 163, 164,
                165, 166, 168, 171, 177, 179, 181,
                182, 183, 184, 185, 186, 187, 188, 191,
                192, 193, 194, 195, 196, 197, 200, 201,
                211,
            ]

            data.forEach(function(feature) {
                for (let i = 0; i < arrayCentro.length; i++) {
                    if (feature.getProperty("CVE_MUN") == arrayCentro[i]) {
                        data.overrideStyle(
                            feature, {
                                strokeWeight: 1,
                                fillColor: 'rgba(323, 161, 27, 0.75)'
                            });
                    }
                };
            });

        });
    }

    function verCentro() {
        let info_window;
        data.addListener("addfeature", function(event) {
            data.revertStyle();
            data.overrideStyle(
                event.feature, {
                    strokeWeight: 2,
                    fillColor: 'rgba(323, 161, 27, 0.75)'
                });

            /*
            if (info_window) {
                info_window.setMap(null);
                info_window = null;
            }

            */

            let arrayCentro = [
                1, 2, 4, 6, 7, 8, 9, 10, 14, 16, 17, 18,
                19, 20, 21, 22, 23, 24, 25, 26, 28, 29, 30, 31,
                36, 38, 41, 42, 43, 44, 46, , 47, 49, 52, 53, 57,
                62, 65, 68, 71, 74, 79, 80, 81, 85, 86,
                87, 88, 90, 92, 93, 95, 96, 98, 99, 100, 101,
                102, 105, 106, 107, 109, 110, 112, 113, 114,
                115, 117, 118, 125, 126, 127, 128,
                132, 134, 135, 136, 137, 138, 140, 146,
                147, 148, 156, 159, 162, 163, 164,
                165, 166, 168, 171, 177, 179, 181,
                182, 183, 184, 185, 186, 187, 188, 191,
                192, 193, 194, 195, 196, 197, 200, 201,
                211,
            ]

            data.forEach(function(feature) {
                for (let i = 0; i < arrayCentro.length; i++) {
                    if (feature.getProperty("CVE_MUN") == arrayCentro[i]) {
                        data.overrideStyle(
                            feature, {
                                strokeWeight: 1,
                                fillColor: 'rgba(323, 161, 27, 0.75)'
                            });
                    }
                };
            });
        });
    }


    /*Veracruz Sur */

    function sur(valor) {
        //alert(veracruz["valor"])
        data = new google.maps.Data();
        data.loadGeoJson(municipios);
        data.setStyle({
            strokeColor: "rgba(0, 0, 0, 0.91)",
            strokeOpacity: 0.8,
            strokeWeight: 0.5,
            fillColor: "rgba(122, 187, 240, 0.7)",
            fillOpacity: 0.5
        });
        data.setMap(map);
        MunicipiosSur(valor);
        verSur();
    }

    function MunicipiosSur(valor) {

        //alert(valor)
        let info_window;
        data.addListener('click', function(event) {
            data.revertStyle();
            data.overrideStyle(
                event.feature, {
                    strokeWeight: 2,
                    fillColor: 'rgba(32, 161, 27, 0.75)'
                });

            if (info_window) {
                info_window.setMap(null);
                info_window = null;
            }

            if ( veracruz["activo"] == false){
                contenidos = 'Total zona Sur: '+ veracruz["TotalZona"];
                let contenido  =
                    '<div id="content">' +
                    '<div id="siteNotice">' +
                    contenidos
                '</div>' +
                '</div>';

                info_window = new google.maps.InfoWindow({
                    content: contenido ,
                    position: event.latLng,
                    map: map
                });

            }else{
                contenidos = 'Total zona Sur: '+ veracruz["valor"];
                let contenido  =
                    '<div id="content">' +
                    '<div id="siteNotice">' +
                    contenidos
                '</div>' +
                '</div>';

                info_window = new google.maps.InfoWindow({
                    content: contenido ,
                    position: event.latLng,
                    map: map
                });


            }









            let arraySur = [
                3, 5, 11, 12, 15, 31, 32, 39, 45, 48, 54, 59,
                61, 75, 70, 73, 77, 82, 84, 89, 91, 94, 97, 104,
                108, 111, 116, 119, 120, 122, 130, 139, 141,
                142, 143, 144, 145, 149, 169, 172, 174,
                176, 178, 181190, 199, 204, 206, 207, 208, 209,
                210, 212
            ]

            data.forEach(function(feature) {
                for (let i = 0; i < arraySur.length; i++) {
                    if (feature.getProperty("CVE_MUN") == arraySur[i]) {
                        data.overrideStyle(
                            feature, {
                                strokeWeight: 1,
                                fillColor: 'rgba(323, 161, 27, 0.75)'
                            });
                    }
                };
            });

        });
    }

    function verSur() {
        let info_window;
        data.addListener("addfeature", function(event) {
            data.revertStyle();
            data.overrideStyle(
                event.feature, {
                    strokeWeight: 2,
                    fillColor: 'rgba(323, 161, 27, 0.75)'
                });

            if (info_window) {
                info_window.setMap(null);
                info_window = null;
            }

            let arraySur = [
                3, 5, 11, 12, 15, 31, 32, 39, 45, 48, 54, 59,
                61, 75, 70, 73, 77, 82, 84, 89, 91, 94, 97, 104,
                108, 111, 116, 119, 120, 122, 130, 139, 141,
                142, 143, 144, 145, 149, 169, 172, 174,
                176, 178, 181190, 199, 204, 206, 207, 208, 209,
                210, 212
            ]

            data.forEach(function(feature) {
                for (let i = 0; i < arraySur.length; i++) {
                    if (feature.getProperty("CVE_MUN") == arraySur[i]) {
                        data.overrideStyle(
                            feature, {
                                strokeWeight: 1,
                                fillColor: 'rgba(323, 161, 27, 0.75)'
                            });
                    }
                };
            });
        });
    }


/*

$(document).on('click', '#regionCentro', function() {
    let valor = document.getElementById('valorC').value;
    veracruz["TotalZona"] = valor;
    veracruz["activo"] = false;

    alert(veracruz["TotalZona"])

    if (data === undefined) {
        centro(map);
    } else {
        data.setMap(null);
        centro(map);
    }

});

$(document).on('click', '#regionNorte', function() {
    let valor = document.getElementById('valorC').value;
    veracruz["TotalZona"] = valor;
    veracruz["activo"] = false;

    //alert(veracruz["TotalZona"])

    if (data === undefined) {
        norte(map);
    } else {
        data.setMap(null);
        norte(map);
    }

});

 */



function todos(map,valor) {
    data = new google.maps.Data();
    data.loadGeoJson(municipios);
    data.setStyle({
        strokeColor: "gba(255, 255, 255, 1)",
        strokeOpacity: 0.8,
        strokeWeight: 0.5,
        fillColor: "rgba(0, 0, 0, 0.6)",
        fillOpacity: 0.5
    });
    data.setMap(map);

    MunicipiosTodos();

    $(document).on('click', '#regionSur', function() {
        let valor = document.getElementById('valor').value;
        veracruz["TotalZona"] = valor;
        veracruz["activo"] = false;


        verSur();


    });


    $(document).on('click', '#regionCentro', function() {
        let valor = document.getElementById('valorC').value;
        veracruz["TotalZona"] = valor;
        veracruz["activo"] = false;

        alert(veracruz["TotalZona"])

        if (data === undefined) {
            centro(map);
        } else {
            data.setMap(null);
            centro(map);
        }

    });

    $(document).on('click', '#regionNorte', function() {
        let valor = document.getElementById('valorC').value;
        veracruz["TotalZona"] = valor;
        veracruz["activo"] = false;

        //alert(veracruz["TotalZona"])

        if (data === undefined) {
            norte(map);
        } else {
            data.setMap(null);
            norte(map);
        }

    });



}

function MunicipiosTodos(valor) {
    let info_window;
    data.addListener('click', function(event) {
        data.revertStyle();
        data.overrideStyle(
            event.feature, {
                strokeWeight: 2,
                fillColor: 'rgba(32, 161, 27, 0.75)'
            });

        if (info_window) {
            info_window.setMap(null);
            info_window = null;
        }
        info_window = new google.maps.InfoWindow({
            content: event.feature.getProperty("NOMGEO"),
            position: event.latLng,
            map: map
        });
    });
}
















